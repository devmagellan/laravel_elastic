<?php

namespace App\Articles;

use App\Article;
use Illuminate\Database\Eloquent\Collection;

class EloquentArticlesRepository implements ArticlesRepository
{
    public function search($query = "")
    {
        return Article::where('body', 'like', "%{$query}%")
            ->orWhere('title', 'like', "%{$query}%")
            ->get();
    }
}