<?php

namespace App\Articles;

use App\Article;
use Elasticsearch\Client;
use Illuminate\Database\Eloquent\Collection;
use function implode;
use function is_a;
use function is_array;
use function strip_tags;

class ElasticsearchArticlesRepository implements ArticlesRepository
{
    private $search;

    public function __construct(Client $client)
    {
        $this->search = $client;
    }

    public function search($query = "")
    {
        $items = $this->searchOnElasticsearch($query);

        //ppre($items);

        return $this->buildCollection($items);
    }

    public function searchOnElasticsearch($query){
        $instance = new Article;
        $items = $this->search->search([
                'index' => 'elastic',
                'type' => 'article',
                "size"=>50,
                'body'=>[
                    'query'=>[
                        "multi_match"=>[
                            "fields"=>["title","body","tags^5","location_name"],
                            "query"=>$query
                        ]
                    ],
                    "highlight" => [
                        "pre_tags"  => "<b>",
                        "post_tags" => "</b>",
                        "fields" => [
                            "tags" => new \stdClass(),
                            "title"=> new \stdClass(),
                            "body" => new \stdClass(),
                            "location_name" => new \stdClass()

                        ]
                    ]]]


        );

        return $items;

    }

    private function searchOnElasticsearchOld($query)
    {
        $instance = new Article;
        $items = $this->search->search([
            'index' => $instance->getSearchIndex(),
            'type' => $instance->getSearchType(),

            'body' => [
                "query" => [
                    "bool" => [

                        "should" => [
                            [
                                "regexp" => [
                                    "tags" => [
                                        "value" => ".{2,8}" . $query . ".*",
                                    ]

                                ],],
                            [
                                "wildcard" => [
                                    "tags" => [
                                        "value" => "*" . $query . "*",
                                        "boost" => 1.0,
                                        "rewrite" => "constant_score"
                                    ]
                                ]
                            ]
                        ],
                    ]
                ], "highlight" => [
                    "fields" => [
                        "tags" => ["type" => "plain"]
                    ]
                ]


            ]]);

        return $items;
    }

    private function buildCollection(array $items)
    {
        /**
         * The data comes in a structure like this:
         *
         * [
         *      'hits' => [
         *          'hits' => [
         *              [ '_source' => 1 ],
         *              [ '_source' => 2 ],
         *          ]
         *      ]
         * ]
         *
         * And we only care about the _source of the documents.
         */
        $hits = array_pluck($items['hits']['hits'], '_source') ?: [];
        $hidline = array_pluck($items['hits']['hits'], 'highlight') ?: [];

        //ppre($hidline);
        //ppre($hits);

        $sources = array_map(function ($source,$hid) {
            if (is_array($hid)) {
                foreach ($hid as $key => $value)
                {
                    if (is_array($value)) {
                        foreach ($value as $v) {
                            $source[$key] = str_replace(strip_tags($v),$v,$source[$key]);
                        }
                    }

                }
            }

            // The hydrate method will try to decode this
            // field but ES gives us an array already.
            if (isset($source['tags'])) {
                $source['tags'] = json_encode($source['tags']);
            }
            return $source;
        }, $hits,$hidline);

        // We have to convert the results array into Eloquent Models.


        return Article::hydrate($sources);
    }
}
