<?php

namespace App\Console\Commands;

use App\Article;
use Elasticsearch\Client;
use Illuminate\Console\Command;
use function print_r;

class ReindexCommand extends Command
{
    protected $name = "search:reindex";
    protected $description = "Indexes all articles to elasticsearch";
    private $search;

    public function __construct(Client $search)
    {
        parent::__construct();

        $this->search = $search;
    }

    public function handle()
    {
        $this->info('Indexing all articles. Might take a while...');

        //// CREATE INDEX FOR SEARCH
        $setting = [
            'analysis' => [
                'analyzer' => [
                    'ngram_analyzer_with_filter' => [
                        'tokenizer' => 'ngram_tokenizer',
                        'filter' => 'lowercase, snowball'
                    ],
                ],
                'tokenizer' => [
                    'ngram_tokenizer' => [
                        'type' => 'nGram',
                        'min_gram' => 3,
                        'max_gram' => 3,
                        'token_chars' => ['letter', 'digit', 'whitespace', 'punctuation', 'symbol']
                    ],

                ],
            ]
        ];

        $param = [
            'index' => 'elastic',
            'body' => [
                'settings' => $setting,
            ]
        ];


        $mapping = [
            'index' =>'elastic',
            'type'=>'article',
            'body'=>[
                'properties'=>[
                        'title' => [
                            'type' => 'text',
                            'analyzer' => "ngram_analyzer_with_filter",
                        ],
                        'body' => [
                            'type' => 'text',
                            'analyzer' => "ngram_analyzer_with_filter",
                        ],
                        'tags' => [
                            'type' => 'text',
                            'analyzer' => "ngram_analyzer_with_filter",
                        ],
                        "location_name"=> [
                            'type' => 'text',
                            'analyzer' => "ngram_analyzer_with_filter",
                        ]
                    ]
                ]


        ];


        if ($this->search->indices()->exists(['index'=>'elastic'])) {
            $this->search->indices()->delete(['index'=>'elastic']);
        }

        $this->search->indices()->create($param);
        $this->search->indices()->putMapping($mapping);


        //////////////////////////


        foreach (Article::cursor() as $model) {

            $mas = $model->toSearchArray();
            $mas['location_name'] = $model->location->location_name;


            $this->search->index([
                'index' => 'elastic',
                'type' => 'article',
                'id' => $model->id,
                'body' => $mas,
            ]);

            // PHPUnit-style feedback
            $this->output->write('.');
        }

        $this->info("nDone!");
    }
}
